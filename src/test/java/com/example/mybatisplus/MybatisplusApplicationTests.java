package com.example.mybatisplus;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.example.mybatisplus.model.domain.Admin;
import com.example.mybatisplus.model.domain.Book;
import com.example.mybatisplus.service.AdminService;
import com.example.mybatisplus.service.BookService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.management.Query;
import java.util.ArrayList;
import java.util.List;

@SpringBootTest
class MybatisplusApplicationTests {

    @Autowired
    private AdminService adminService;

    @Autowired
    private BookService bookService;


    @Test
    void contextLoads() {
        QueryWrapper<Book> wrapper = new QueryWrapper<>();
        wrapper.like("name","三国").eq("author","罗贯中")
                    .or().eq("id",1);
        List<Book> list = bookService.list(wrapper);
        System.out.println(list);


//        Admin byId = adminService.getById(1);
//        System.out.println(byId);

//        Book byId1 = bookService.getById(1);
//        System.out.println(byId1);
    }

    @Test
    void hh(){
        //add
        Book book = new Book().setAuthor("罗贯中").setName("西游记");
        bookService.save(book);

    }
}
