package com.example.mybatisplus.web.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.stereotype.Controller;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.example.mybatisplus.common.JsonResponse;
import com.example.mybatisplus.service.BookService;
import com.example.mybatisplus.model.domain.Book;


/**
 *
 *  前端控制器
 *
 *
 * @author zxd
 * @since 2021-07-02
 * @version v1.0
 */
@Controller
@RequestMapping("/book")
public class BookController {

    private final Logger logger = LoggerFactory.getLogger( BookController.class );

    @Autowired
    private BookService bookService;

    /**
    * 描述：根据Id 查询
    *
    */
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    public JsonResponse getById(@PathVariable("id") Long id)throws Exception {
        Book  book =  bookService.getById(id);
        return JsonResponse.success(book);
    }

    /**
    * 描述：根据Id删除
    *
    */
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public JsonResponse deleteById(@PathVariable("id") Long id) throws Exception {
        bookService.removeById(id);
        return JsonResponse.success(null);
    }


    /**
    * 描述：根据Id 更新
    *
    */
    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    @ResponseBody
    public JsonResponse updateBook(@PathVariable("id") Long  id,Book  book) throws Exception {
        book.setId(id);
        bookService.updateById(book);
        return JsonResponse.success(null);
    }


    /**
    * 描述:创建Book
    *
    */
    @RequestMapping(value = "", method = RequestMethod.POST)
    @ResponseBody
    public JsonResponse create(Book  book) throws Exception {
        bookService.save(book);
        return JsonResponse.success(null);
    }
}

