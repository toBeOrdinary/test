package com.example.mybatisplus.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class UserController {

    @RequestMapping("/user/user/list")
    public String toList(){
        return "views/user/user/list";
    }


    /**
     *
     * @return 所有用户
     */
    @RequestMapping("/list")
    @ResponseBody
    public String list(){

        return null;
    }

}
