package com.example.mybatisplus.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class IndexController {

    @RequestMapping("/")
    public String toLogin(){
        return "login";
    }
    @RequestMapping("/index")
    public String toIndex(){
        return "index";
    }
    @RequestMapping("/console")
    public String console(){
        return "views/home/console";
    }

}
