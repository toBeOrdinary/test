package com.example.mybatisplus.mapper;

import com.example.mybatisplus.model.domain.Category;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author lxp
 * @since 2021-04-21
 */
public interface CategoryMapper extends BaseMapper<Category> {

}
