package com.example.mybatisplus.mapper;

import com.example.mybatisplus.model.domain.Book;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zxd
 * @since 2021-07-02
 */
public interface BookMapper extends BaseMapper<Book> {

}
