package com.example.mybatisplus.service;

import com.example.mybatisplus.model.domain.Book;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zxd
 * @since 2021-07-02
 */
public interface BookService extends IService<Book> {

}
