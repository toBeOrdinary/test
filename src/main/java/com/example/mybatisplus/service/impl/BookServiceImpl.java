package com.example.mybatisplus.service.impl;

import com.example.mybatisplus.model.domain.Book;
import com.example.mybatisplus.mapper.BookMapper;
import com.example.mybatisplus.service.BookService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zxd
 * @since 2021-07-02
 */
@Service
public class BookServiceImpl extends ServiceImpl<BookMapper, Book> implements BookService {

}
