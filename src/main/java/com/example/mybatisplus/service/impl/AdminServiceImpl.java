package com.example.mybatisplus.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.example.mybatisplus.model.domain.Admin;
import com.example.mybatisplus.mapper.AdminMapper;
import com.example.mybatisplus.service.AdminService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author lxp
 * @since 2021-04-19
 */
@Service
public class AdminServiceImpl extends ServiceImpl<AdminMapper, Admin> implements AdminService {

    @Autowired
    private AdminMapper adminMapper;

    @Override
    public Admin login(String username, String password) {
        QueryWrapper<Admin> wrapper = new QueryWrapper<>();
        wrapper.eq("loginName",username).eq("password",password);

        List<Admin> admins = adminMapper.selectList(wrapper);
        return admins.size()>0?admins.get(0):null;
    }

    @Override
    public List<Admin> listAll() {
        QueryWrapper<Admin> wrapper = new QueryWrapper<>();

        List<Admin> admins = adminMapper.selectList(wrapper);
        return admins.size()==0?null:admins;
    }

    @Override
    public Admin registerNameCheck(String username) {
        QueryWrapper<Admin> wrapper = new QueryWrapper<>();
        wrapper.eq("loginName",username);
        List<Admin> admins = adminMapper.selectList(wrapper);
        return admins.size()>0?admins.get(0):null;
    }


    @Override
    public int register(String username, String password) {
        Admin admin = new Admin();
        admin.setLoginName(username).setPassword(password);
        int i = adminMapper.insert(admin);

        return i;
    }
}
