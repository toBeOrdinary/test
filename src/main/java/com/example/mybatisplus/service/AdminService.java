package com.example.mybatisplus.service;

import com.example.mybatisplus.model.domain.Admin;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lxp
 * @since 2021-04-19
 */
public interface AdminService extends IService<Admin> {

    Admin login(String username, String password);

    List<Admin> listAll();

    Admin registerNameCheck(String username);

    int register(String username, String password);
}
