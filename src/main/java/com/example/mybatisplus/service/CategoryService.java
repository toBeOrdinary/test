package com.example.mybatisplus.service;

import com.example.mybatisplus.model.domain.Category;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lxp
 * @since 2021-04-21
 */
public interface CategoryService extends IService<Category> {

}
